#ifndef FILE_ZLIB_576
#define FILE_ZLIB_576

#include <stdio.h>
#include "zlib.h"

namespace file_zlib {

/* Compress from file source to file dest until EOF on source.
   function returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_STREAM_ERROR if an invalid compression
   level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
   version of the library linked do not match, or Z_ERRNO if there is
   an error reading or writing the files. */
int compress(FILE *source, FILE *dest, int level = Z_DEFAULT_COMPRESSION);

}

#endif
