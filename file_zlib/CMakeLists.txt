cmake_minimum_required(VERSION 3.1)

project(FileZlib)

find_package(ZLIB REQUIRED)

add_library(file_zlib STATIC file_zlib.hpp file_zlib.cpp)

target_link_libraries(file_zlib ZLIB::ZLIB)


