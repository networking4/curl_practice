# CMAKE Practice

Using cmake add libcurl and zlib to project to perform some task

# Task

1. Download file from [here](https://knureigs.github.io/itech/lb/ITech2_PDO/Additional/iteh2lb1var2.sql) to the current process directory and name it dump.sql

2. Compress file dump.sql into zip format and name compressed file as dump.sql.zip

3. Delete first uploaded file dump.sql

# Links

The function which compress file was taken from [here](https://zlib.net/zlib_how.html)

# Results

Well, the file size really reduced, but it cannot be unzipped/uncompressed using standard utilities.  Therefore the process of decompressing should also be added to project. The function which decompress file can be found [here](https://zlib.net/zlib_how.html)

I had to use [gzip lib from boost](https://www.boost.org/doc/libs/1_46_1/libs/iostreams/doc/classes/gzip.html) or something else that add headers.
