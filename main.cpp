#include <cstring>
#include <iostream>
using std::cout;
#include <curl/curl.h>
#include "file_zlib.hpp"


size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

int main(void) {
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char url[] = "https://knureigs.github.io/itech/lb/ITech2_PDO/Additional/iteh2lb1var2.sql";
    char outfilename[] = "./dump.sql";
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(outfilename,"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        /* always cleanup */
        curl_easy_cleanup(curl);
        fclose(fp);
    }

    // uploaded file name
    char upl_fname[] = "dump.sql";

    FILE* src = fopen(upl_fname, "r");

    // compressed file name
    char comp_fname[50];
    std::strcpy(comp_fname, upl_fname);
    std::strcat(comp_fname, ".zip");

    FILE* dest = fopen(comp_fname, "w");

    int ret = file_zlib::compress(src, dest);
    if (ret != Z_OK)
        cout << "Error in main: ret != Z_OK";

    fclose(src);
    fclose(dest);

    /*	Deletes the file dump.sql */
    if (std::remove(upl_fname) != 0)
            perror("File deletion failed\n");
    else
        cout << "Should be success.\n";

    return ret;
}
